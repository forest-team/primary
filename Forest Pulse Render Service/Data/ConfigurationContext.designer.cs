﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ForestPulseRenderService.Data
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	public partial class ConfigurationContextDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertSettings(Settings instance);
    partial void UpdateSettings(Settings instance);
    partial void DeleteSettings(Settings instance);
    #endregion
		
		public ConfigurationContextDataContext() : 
				base(global::ForestPulseRenderService.Properties.Settings.Default.ConfigurationConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public ConfigurationContextDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ConfigurationContextDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ConfigurationContextDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ConfigurationContextDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Settings> Settings
		{
			get
			{
				return this.GetTable<Settings>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	[global::System.Data.Linq.Mapping.InheritanceMappingAttribute(Code="\"Bool\"", Type=typeof(SettingsBool))]
	[global::System.Data.Linq.Mapping.InheritanceMappingAttribute(Code="\"String\"", Type=typeof(SettingsString))]
	[global::System.Data.Linq.Mapping.InheritanceMappingAttribute(Code="\"Int\"", Type=typeof(SettingsInt))]
	public partial class Settings : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _Key;
		
		private string _Type;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnKeyChanging(string value);
    partial void OnKeyChanged();
    partial void OnTypeChanging(string value);
    partial void OnTypeChanged();
    #endregion
		
		public Settings()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Key", CanBeNull=false, IsPrimaryKey=true)]
		public string Key
		{
			get
			{
				return this._Key;
			}
			set
			{
				if ((this._Key != value))
				{
					this.OnKeyChanging(value);
					this.SendPropertyChanging();
					this._Key = value;
					this.SendPropertyChanged("Key");
					this.OnKeyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Type", CanBeNull=false, IsDiscriminator=true)]
		public string Type
		{
			get
			{
				return this._Type;
			}
			set
			{
				if ((this._Type != value))
				{
					this.OnTypeChanging(value);
					this.SendPropertyChanging();
					this._Type = value;
					this.SendPropertyChanged("Type");
					this.OnTypeChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	public partial class SettingsBool : Settings
	{
		
		private bool _ValueBool;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnValueBoolChanging(bool value);
    partial void OnValueBoolChanged();
    #endregion
		
		public SettingsBool()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ValueBool")]
		public bool ValueBool
		{
			get
			{
				return this._ValueBool;
			}
			set
			{
				if ((this._ValueBool != value))
				{
					this.OnValueBoolChanging(value);
					this.SendPropertyChanging();
					this._ValueBool = value;
					this.SendPropertyChanged("ValueBool");
					this.OnValueBoolChanged();
				}
			}
		}
	}
	
	public partial class SettingsString : Settings
	{
		
		private string _ValueString;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnValueStringChanging(string value);
    partial void OnValueStringChanged();
    #endregion
		
		public SettingsString()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ValueString", CanBeNull=false)]
		public string ValueString
		{
			get
			{
				return this._ValueString;
			}
			set
			{
				if ((this._ValueString != value))
				{
					this.OnValueStringChanging(value);
					this.SendPropertyChanging();
					this._ValueString = value;
					this.SendPropertyChanged("ValueString");
					this.OnValueStringChanged();
				}
			}
		}
	}
	
	public partial class SettingsInt : Settings
	{
		
		private int _ValueInt;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnValueIntChanging(int value);
    partial void OnValueIntChanged();
    #endregion
		
		public SettingsInt()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ValueInt")]
		public int ValueInt
		{
			get
			{
				return this._ValueInt;
			}
			set
			{
				if ((this._ValueInt != value))
				{
					this.OnValueIntChanging(value);
					this.SendPropertyChanging();
					this._ValueInt = value;
					this.SendPropertyChanged("ValueInt");
					this.OnValueIntChanged();
				}
			}
		}
	}
}
#pragma warning restore 1591
