using System;
using System.Linq;

namespace ForestPulseRenderService.Data.SettingsProvider
{
    public abstract class SettingsProvider<T> : ISettingsProvider<T>
    {
        protected ConfigurationContextDataContext Configuration { get; set;  }
        protected abstract string KeyType { get; }

        protected abstract Func<Settings, T> ValueSelector { get; }

        protected SettingsProvider()
        {
            Configuration = new ConfigurationContextDataContext();
        }

        public T Get(string key)
        {
            var result = Configuration.Settings.FirstOrDefault(s => s.Key == key && s.Type == KeyType);

            if (result != null) return ValueSelector(result);
            
            throw new ArgumentOutOfRangeException("key", string.Format("Setting now found with key '{0}' and type '{1}'", key, KeyType));
        }

        public abstract void Set(string key, T value);

        public bool Exists(string key)
        {
            return Configuration.Settings.Any(s => s.Key == key && s.Type == KeyType);
        }

        public void Remove(string key)
        {
            var setting = Configuration.Settings.FirstOrDefault(s => s.Key == key && s.Type == KeyType);            
            if (setting != null) Configuration.Settings.DeleteOnSubmit(setting);
            Configuration.SubmitChanges();
        }        
    }
}