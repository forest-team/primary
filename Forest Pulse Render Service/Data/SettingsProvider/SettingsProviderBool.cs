﻿using System;
using System.Linq;

namespace ForestPulseRenderService.Data.SettingsProvider
{
    public class SettingsProviderBool : SettingsProvider<bool>
    {
        protected override string KeyType { get { return @"Bool"; } }

        protected override Func<Settings, bool> ValueSelector { get { return s => ((SettingsBool)s).ValueBool; } }

        public override void Set(string key, bool value)
        {
            var setting = Configuration.Settings.FirstOrDefault(s => s.Key == key && s.Type == KeyType);

            if (setting == null)
            {
                Configuration.Settings.InsertOnSubmit(new SettingsBool() { Key = key, Type = KeyType, ValueBool = value });
            }
            else
            {
                ((SettingsBool)setting).ValueBool = value;
            }

            Configuration.SubmitChanges();
        }
    }
}