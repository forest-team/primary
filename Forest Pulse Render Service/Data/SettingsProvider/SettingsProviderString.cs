﻿using System;
using System.Linq;

namespace ForestPulseRenderService.Data.SettingsProvider
{
    public class SettingsProviderString : SettingsProvider<string>
    {
        protected override string KeyType { get { return @"String"; } }

        protected override Func<Settings, string> ValueSelector { get { return s => ((SettingsString)s).ValueString; } }

        public override void Set(string key, string value)
        {
            var setting = Configuration.Settings.FirstOrDefault(s => s.Key == key && s.Type == KeyType);

            if (setting == null)
            {
                Configuration.Settings.InsertOnSubmit(new SettingsString() { Key = key, Type = KeyType, ValueString = value });
            }
            else
            {
                ((SettingsString)setting).ValueString = value;
            }

            Configuration.SubmitChanges();
        }
    }
}