﻿namespace ForestPulseRenderService.Data.SettingsProvider
{
    public interface ISettingsProvider<T>
    {
        T Get(string key);
        void Set(string key, T value);
        bool Exists(string key);
        void Remove(string key);
    }
}
