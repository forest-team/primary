﻿using System;
using System.Linq;

namespace ForestPulseRenderService.Data.SettingsProvider
{
    public class SettingsProviderInt : SettingsProvider<int>
    {
        protected override string KeyType { get { return @"Int"; } }

        protected override Func<Settings, int> ValueSelector { get { return s => ((SettingsInt) s).ValueInt; } }
        public override void Set(string key, int value)
        {
            var setting =  Configuration.Settings.FirstOrDefault(s => s.Key == key && s.Type == KeyType);

            if (setting == null)
            {
                Configuration.Settings.InsertOnSubmit(new SettingsInt() {Key = key, Type = KeyType, ValueInt = value});
            }
            else
            {
                ((SettingsInt) setting).ValueInt = value;
            }

            Configuration.SubmitChanges();
        }
    }
}