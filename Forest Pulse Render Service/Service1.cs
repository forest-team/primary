﻿using System.ServiceProcess;
using ForestPulseRenderService.Plugins;

namespace ForestPulseRenderService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            var loader = new PluginLoader();
            
        }

        protected override void OnStop()
        {
        }
    }
}
