﻿using System;

namespace ForestPulseRenderService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

#if DEBUG
            var service = new Service1();
            service.OnDebug();
            Console.WriteLine("Prease any key to shutdown rendering service");
            Console.ReadKey();
#else
            var servicesToRun = new ServiceBase[] 
            { 
                new Service1()
            };
            ServiceBase.Run(servicesToRun);
#endif
            
        }
    }
}
