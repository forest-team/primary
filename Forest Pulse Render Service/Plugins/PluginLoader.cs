﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using ForestPulseShared;

namespace ForestPulseRenderService.Plugins
{
    public class PluginLoader
    {
        protected CompositionContainer Container { get; set; }
        public IEnumerable<Lazy<IServerPlugin>> Plugins { get; protected set; }

        public PluginLoader()
        {
            var aggCatalog = new AggregateCatalog();
            
            aggCatalog.Catalogs.Add(new AssemblyCatalog(typeof(PluginLoader).Assembly));            
            DirSearch(AppDomain.CurrentDomain.BaseDirectory + "\\Plugins", aggCatalog);            

            Container = new CompositionContainer(aggCatalog);

            try
            {
                Plugins = Container.GetExports<IServerPlugin>();

                foreach (var plugin in Plugins)
                {
                    Console.WriteLine("Found plugin {0}", plugin.Value.Name);                    
                }
            }
            catch (CompositionException compositionException)
            {                
                Console.WriteLine(compositionException.ToString());
            }   
        }

        private static void DirSearch(string sDir, AggregateCatalog catalog)
        {
            foreach (string d in Directory.GetDirectories(sDir))
            {
                catalog.Catalogs.Add(new DirectoryCatalog(d));
                DirSearch(d, catalog);
            }
        }
    }
}
