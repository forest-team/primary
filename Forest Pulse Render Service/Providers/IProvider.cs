﻿namespace ForestPulseRenderService.Providers
{
    public interface IProvider
    {
    }

    public interface IInputOutputProvider : IProvider
    {
        
    }
}
