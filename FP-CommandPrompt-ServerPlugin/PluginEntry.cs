﻿using System;
using System.ComponentModel.Composition;
using ForestPulseShared;

namespace ForestPulse.ServerPlugin.CommandPrompt
{
    [Export(typeof(IServerPlugin))]
    public class PluginEntry : ServerPluginBase
    {
        public override string Name { get { return "Command Prompt"; } }
        public override PluginType PluginType { get {return PluginType.Utility;} }
    }
}
