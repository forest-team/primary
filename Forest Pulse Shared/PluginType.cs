﻿using System;

namespace ForestPulseShared
{
    [Flags]
    public enum PluginType
    {
        Utility,
        AudioInput,
        AudioManipulation,
        VisualInput,
        VisualManipulation,
        VisualOutput
    }
}