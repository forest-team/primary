﻿using System;

namespace ForestPulseShared
{
    public interface IPlugin
    {
        string Name { get; }
        PluginType PluginType { get; }
        Version Version { get; }
    }

    public interface IServerPlugin : IPlugin { }

    public interface IClientPlugin : IPlugin { }

    public abstract class ServerPluginBase : IServerPlugin
    {
        public abstract string Name { get; }
        public abstract PluginType PluginType { get; }
        public virtual Version Version { get { return GetType().Assembly.GetName().Version; } }
    }

    public abstract class ClientPluginBase : IClientPlugin
    {
        public abstract string Name { get; }
        public abstract PluginType PluginType { get; }
        public virtual Version Version { get { return GetType().Assembly.GetName().Version; } }
    }

}
